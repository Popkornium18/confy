from __future__ import annotations

import shlex
import shutil
import tempfile
from pathlib import Path
from subprocess import run

import pytest

from confy.main import GitRepo
from confy.main import parse_config
from confy.util import backup_file_name

test_data_dir = Path(__file__).resolve().parent / "testdata"


@pytest.fixture
def git_repo(tmp_path: Path) -> GitRepo:
    valid_git_repo_config = test_data_dir / "valid_git_repo.toml"
    config_copy = tmp_path / valid_git_repo_config.name
    shutil.copy(valid_git_repo_config, config_copy)
    config = parse_config(config_copy)
    assert len(config.configs) == 1
    app_config = config.configs[0]
    assert isinstance(app_config, GitRepo)
    return app_config


def test_git_repo_remote(git_repo: GitRepo):
    git_repo.update()
    assert git_repo.remote == "https://gitlab.com/Popkornium18/nvim.git"


def test_git_repo_remote_not_initialized(git_repo: GitRepo):
    with pytest.raises(Exception) as exc_info:
        git_repo.remote

    assert str(exc_info.value) == f"Git repo {str(git_repo.git_dir)!r} is not initialized"


def test_git_repo_no_remote(git_repo: GitRepo):
    git_repo.git_dir.mkdir(parents=True)
    run(f"git -C {shlex.quote(str(git_repo.git_dir))} init &> /dev/null", shell=True)
    with pytest.raises(Exception) as exc_info:
        git_repo.remote

    assert str(exc_info.value) == f"Git repo {str(git_repo.git_dir)!r} has no origin"


def test_git_repo_not_a_git_repo(git_repo: GitRepo):
    git_repo.git_dir.mkdir(parents=True)
    with pytest.raises(Exception) as exc_info:
        git_repo.remote

    assert str(exc_info.value) == f"Not a git repo: {str(git_repo.git_dir)!r}"


def test_git_repo_invalid_args():
    with pytest.raises(Exception) as exc_info_1:
        GitRepo(repo="don't care", git_dir=Path("/ab/so/lute"), destination=Path("not/absolute"))
    with pytest.raises(Exception) as exc_info_2:
        GitRepo(repo="don't care", git_dir=Path("not/absolute"), destination=Path("/ab/so/lute"))

    assert str(exc_info_1.value) == str(exc_info_2.value) == "'not/absolute' is not an absolute path"


def test_valid_git_repo(git_repo: GitRepo):
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir


def test_valid_git_repo_dead_symlink_exists(git_repo: GitRepo):
    invalid = Path("/does/not/exist")
    assert not invalid.exists() and not invalid.is_symlink()
    git_repo.destination.parent.mkdir(parents=True)
    git_repo.destination.symlink_to(invalid)
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir


def test_valid_git_repo_symlink_exists(git_repo: GitRepo):
    git_repo.destination.parent.mkdir(parents=True)
    git_repo.destination.symlink_to(Path("/etc/passwd"))
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir


def test_valid_git_repo_dir_exists(git_repo: GitRepo):
    git_repo.destination.mkdir(parents=True)
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir
    assert (git_repo.destination.parent / f"confy_backup_{git_repo.destination.name}").is_dir()


def test_valid_git_repo_file_exists(git_repo: GitRepo):
    git_repo.destination.parent.mkdir(parents=True)
    git_repo.destination.touch()
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir
    assert (git_repo.destination.parent / f"confy_backup_{git_repo.destination.name}").is_file()


def test_valid_git_repo_file_backup_exists(git_repo: GitRepo):
    git_repo.destination.parent.mkdir(parents=True)
    git_repo.destination.touch()
    backup = git_repo.destination.parent / f"confy_backup_{git_repo.destination.name}"
    backup.touch()
    git_repo.update()
    assert git_repo.destination.is_symlink()
    assert git_repo.destination.resolve() == git_repo.git_dir
    assert (git_repo.destination.parent / f"confy_backup_1_{git_repo.destination.name}").is_file()


def test_backup_dir_name():
    with tempfile.TemporaryDirectory() as dir:
        path = Path(dir) / "somedir"
        path.mkdir(exist_ok=False)
        prefix = "test_"
        backup = backup_file_name(path, prefix=prefix)
        assert backup.name == f"{prefix}{path.name}"

        for i in range(1, 11):
            backup.mkdir()
            backup = backup_file_name(path, prefix=prefix)
            assert backup.name == f"{prefix}{i}_{path.name}"


def test_backup_file_name():
    with tempfile.TemporaryDirectory() as dir:
        path = Path(dir) / "somefile"
        path.touch(exist_ok=False)
        prefix = "test_"
        backup = backup_file_name(path, prefix=prefix)
        assert backup.name == f"{prefix}{path.name}"

        for i in range(1, 11):
            backup.touch()
            backup = backup_file_name(path, prefix=prefix)
            assert backup.name == f"{prefix}{i}_{path.name}"


def test_config_file_not_found():
    with pytest.raises(FileNotFoundError):
        parse_config(Path("/does/not/exist"))

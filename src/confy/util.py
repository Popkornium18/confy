from __future__ import annotations

import itertools
from pathlib import Path


def backup_file_name(path: Path, prefix: str = "confy_backup_") -> Path:
    backup = path.parent / f"{prefix}{path.name}"
    if not backup.exists():
        return backup

    for i in itertools.count(1):
        backup = path.parent / f"{prefix}{i}_{path.name}"
        if not backup.exists():
            return backup

    raise Exception(f"Could not generate backup file name for {str(path)!r}")  # pragma: no cover

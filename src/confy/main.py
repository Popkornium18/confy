from __future__ import annotations

import argparse
import errno
import os
import tomllib
from dataclasses import dataclass
from pathlib import Path
from string import Template
from typing import Literal
from typing import NamedTuple
from typing import Protocol

from confy.git_repo import GitRepo


@dataclass(frozen=True)
class ConfyConfig:
    source_dir: Path
    configs: tuple[InstallableConfig, ...]


class InstallableConfig(Protocol):
    def update(self) -> None:
        ...


class ConfyArgs(NamedTuple):
    command: Literal["update"]
    config: Path


def parse_config(file: Path) -> ConfyConfig:
    if not file.is_file():
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), str(file))

    with open(file, mode="rb") as toml_file:
        data = tomllib.load(toml_file)

    source_dir = file.parent.resolve()
    configs: list[InstallableConfig] = []

    for name, config in data.items():
        if "source_repo" in config:
            try:
                source_repo: str = config["source_repo"]
                link_dir: str = config["link_dir"]
            except KeyError as e:
                raise Exception(f"Invalid connfig: {str(e)}")

            destination = Path(Template(link_dir).substitute(os.environ))
            if not destination.is_absolute():
                destination = (source_dir / destination).absolute()

            config = GitRepo(repo=source_repo, destination=destination, git_dir=source_dir / name)
        else:
            raise Exception(f"{name}: Unsupported config type")

        configs.append(config)

    if not configs:
        raise Exception(f"{str(file)}: No configs found")

    return ConfyConfig(source_dir=source_dir, configs=tuple(configs))


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="confy", description="Configuration manager")
    parser.add_argument(
        "-c",
        "--config",
        type=Path,
        metavar="FILE",
        help="Specify the configuration file",
        default=Path.cwd() / "confy.toml",
    )

    subparsers = parser.add_subparsers(dest="command", title="Available commands", required=True)

    subparsers.add_parser("update", help="Update all configs")
    return parser


def main() -> None:
    parser = create_parser()
    args = ConfyArgs(**vars(parser.parse_args()))
    config = parse_config(args.config)

    if args.command == "update":
        for tool_config in config.configs:
            tool_config.update()


if __name__ == "__main__":
    main()

from __future__ import annotations

import functools
import re
import shlex
import subprocess as sp
import sys
from dataclasses import dataclass
from pathlib import Path

from confy.util import backup_file_name


run = functools.partial(sp.run, capture_output=True, check=True)
print_err = functools.partial(print, file=sys.stderr)


@dataclass
class GitRepo:
    repo: str
    git_dir: Path
    destination: Path

    def __post_init__(self) -> None:
        for path in [self.git_dir, self.destination]:
            if not path.is_absolute():
                raise Exception(f"{str(path)!r} is not an absolute path")

    @functools.cached_property
    def remote(self) -> str:
        if not self.git_dir.is_dir():
            raise Exception(f"Git repo {str(self.git_dir)!r} is not initialized")

        try:
            command = f"git -C {shlex.quote(str(self.git_dir))} remote -v"
            result = run(shlex.split(command))
        except sp.CalledProcessError:
            raise Exception(f"Not a git repo: {str(self.git_dir)!r}")

        for line in [l for l in result.stdout.decode().split("\n") if l]:
            name, repo, _ = re.split(pattern=" |\t", string=line)
            if name == "origin":
                return repo

        raise Exception(f"Git repo {str(self.git_dir)!r} has no origin")

    def _make_symlink(self):
        self.destination.symlink_to(target=self.git_dir, target_is_directory=True)
        print(f"Installed config to {self.destination}")

    def _clone_or_pull_repo(self) -> None:
        if not self.git_dir.exists():
            print(f"Cloning {self.repo!r} into {str(self.git_dir)!r}")
            command = f"git clone {shlex.quote(self.repo)} {shlex.quote(str(self.git_dir))}"
            run(shlex.split(command))
            return

        if self.remote != self.repo:
            raise Exception(f"Git repo {str(self.git_dir)!r} exists, but it's remote is not {self.repo!r}")

        print(f"Pulling {str(self.git_dir)!r}")
        command = f"git -C {shlex.quote(str(self.git_dir))} pull"
        run(shlex.split(command))

    def update(self) -> None:
        self._clone_or_pull_repo()

        if self.destination.is_symlink() and self.destination.resolve() == self.git_dir:
            return

        if not self.destination.parent.is_dir():
            self.destination.parent.mkdir(parents=True)

        if self.destination.is_symlink():
            self.destination.unlink()
            self._make_symlink()
        elif not self.destination.exists():
            self._make_symlink()
        elif self.destination.is_dir() or self.destination.is_file():
            backup = backup_file_name(self.destination)
            self.destination.rename(backup)
            print_err(f"Moved currently installed config to {str(backup)!r}")
            self._make_symlink()
        else:  # pragma: no cover
            raise Exception(f"{str(self.destination)!r} exists, but we don't know how to handle it")

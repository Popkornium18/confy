# Confy

Confy is a dotfile manager. TODO: Docs

## Setup

Make sure that you configured poetry to create the virtualenv in the project directory.

```
poetry config virtualenvs.in-project true
```

Then create the environment and install the project.
```
poetry shell
poetry install
```

After that, make sure to install the pre-commit hooks.

```
pre-commit install --install-hooks
```
